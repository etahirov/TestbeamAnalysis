#!/bin/python

from ROOT import *

# ASIC numbers
asics = [1,2,3,4,5,6,7,8,9,10]

# dictionary to convert DAC -> mV
#pp=[1311.09,3.52,-613.88] #LS3 SS 500V
pp=[1394.17,3.68,-654.56] #LS3 LS 500V
#pp=[1408.69,3.90,-666.84] #LS4 ASIC 16
#pp=[1463.4,4.24,-687.65] #LS4 ASIC 17
#pp=[1365.46,3.73,-643.83] #LS4 ASIC 18
#pp=[1547.85,4.49,-729.54] #LS4 ASIC 19
#pp=[1500.34,4.10,-711.41] #LS4 ASIC 20
#pp=[1498.97,3.86,-715.83] #LS4 ASIC 21
#pp=[1640.39,4.78,-775.47] #LS4 ASIC 22
#pp=[1479.22,4.21,-693.05] #LS4 ASIC 23
#pp=[1443.06,3.86,-681.63] #LS4 ASIC 24
mV = {20: -pp[1]*TMath.Log(pp[0]/(67.572-pp[2])-1), 30: -pp[1]*TMath.Log(pp[0]/(95.7241-pp[2])-1), 38: -pp[1]*TMath.Log(pp[0]/(118.163-pp[2])-1),40: -pp[1]*TMath.Log(pp[0]/(123.756-pp[2])-1), 45: -pp[1]*TMath.Log(pp[0]/(137.714-pp[2])-1), 52: -pp[1]*TMath.Log(pp[0]/(157.197-pp[2])-1), 60: -pp[1]*TMath.Log(pp[0]/(179.395-pp[2])-1), 68: -pp[1]*TMath.Log(pp[0]/(201.53-pp[2])-1), 70: -pp[1]*TMath.Log(pp[0]/(207.055-pp[2])-1), 75: -pp[1]*TMath.Log(pp[0]/(220.854-pp[2])-1),80: -pp[1]*TMath.Log(pp[0]/(234.633-pp[2])-1), 85: -pp[1]*TMath.Log(pp[0]/(248.395-pp[2])-1),90: -pp[1]*TMath.Log(pp[0]/(262.14-pp[2])-1),95: -pp[1]*TMath.Log(pp[0]/(275.869-pp[2])-1), 105: -pp[1]*TMath.Log(pp[0]/(303.272-pp[2])-1), 115: -pp[1]*TMath.Log(pp[0]/(330.595-pp[2])-1), 120: -pp[1]*TMath.Log(pp[0]/(344.211-pp[2])-1),125: -pp[1]*TMath.Log(pp[0]/(357.78-pp[2])-1), 130: -pp[1]*TMath.Log(pp[0]/(371.271-pp[2])-1),135: -pp[1]*TMath.Log(pp[0]/(384.642-pp[2])-1),140: -pp[1]*TMath.Log(pp[0]/(397.827-pp[2])-1),150: -pp[1]*TMath.Log(pp[0]/(423.415-pp[2])-1), 165: -pp[1]*TMath.Log(pp[0]/(459.626-pp[2])-1), 175: -pp[1]*TMath.Log(pp[0]/(482.408-pp[2])-1),180: -pp[1]*TMath.Log(pp[0]/(493.37-pp[2])-1) }

def error_function(func_name,min,max):
    func = TF1(func_name,"[0]*0.5*TMath::Erfc((x-[1])/(TMath::Sqrt(2)*[2])*(1+0.6*(TMath::Exp(-[3]*(x-[1])/(TMath::Sqrt(2)*[2]))-TMath::Exp([3]*(x-[1])/(TMath::Sqrt(2)*[2]))) / (TMath::Exp(-[3]*(x-[1])/(TMath::Sqrt(2)*[2]))+TMath::Exp([3]*(x-[1])/(TMath::Sqrt(2)*[2])))))",min,max)
    func.SetParameter(0,1)       # efficiency
    func.SetParLimits(0,0,1.0)
    func.SetParameter(1,max*0.4) # median charge
    func.SetParameter(2,1.5)     # width (sigma)
    func.SetParameter(3,0.5)     # skew
    return func

def set_atlas_style():

    gStyle.SetOptStat(0)
    gStyle.SetPadTopMargin(0.05)
    gStyle.SetPadRightMargin(0.05)
    gStyle.SetPadBottomMargin(0.14)
    gStyle.SetPadLeftMargin(0.14)
    gStyle.SetTitleYOffset(1.2)
    gStyle.SetTitleXOffset(1.2)

    gStyle.SetMarkerStyle(20)
    gStyle.SetLineColor(kBlack)

    font = 42
    tsize = 0.05
    gStyle.SetTextFont(font)

    gStyle.SetTextSize(tsize)
    gStyle.SetLabelFont(font,"x")
    gStyle.SetTitleFont(font,"x")
    gStyle.SetLabelFont(font,"y")
    gStyle.SetTitleFont(font,"y")
    gStyle.SetLabelFont(font,"z")
    gStyle.SetTitleFont(font,"z")

    gStyle.SetLabelSize(tsize,"x")
    gStyle.SetTitleSize(tsize,"x")
    gStyle.SetLabelSize(tsize,"y")
    gStyle.SetTitleSize(tsize,"y")
    gStyle.SetLabelSize(tsize,"z")
    gStyle.SetTitleSize(tsize,"z")

    gStyle.SetPalette(1)
    gStyle.SetNumberContours(100)

    TH1.SetDefaultSumw2(kTRUE)
